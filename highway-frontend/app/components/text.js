import React, { Component } from 'react';
import { View, Text } from 'react-native';
import {styles} from './styles';
import { DEFAULT_TEXT_COLOR, DEFAULT_FONT, HLG_SIZE, HREG_SIZE, HSM_SIZE, PLG_SIZE, PREG_SIZE, PSM_SIZE } from './common';

export class HLg extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <Text style={[styles.hlg,{color:this.props.color?this.props.color:DEFAULT_TEXT_COLOR,fontSize:this.props.fontSize?this.props.fontSize:HLG_SIZE,fontFamily:this.props.fontFamily?this.props.fontFamily:DEFAULT_FONT,textAlign:this.props.textAlign?this.props.textAlign:'center',fontWeight:this.props.fontWeight?this.props.fontWeight:'normal'}]}>
          {this.props.children}
      </Text>
    );
  }
}

export class HReg extends Component {
    constructor(props) {
      super(props);
    }
  
    render() {
      return (
        <Text style={[styles.hreg,{color:this.props.color?this.props.color:DEFAULT_TEXT_COLOR,fontSize:this.props.fontSize?this.props.fontSize:HREG_SIZE,fontFamily:this.props.fontFamily?this.props.fontFamily:DEFAULT_FONT,textAlign:this.props.textAlign?this.props.textAlign:'center',fontWeight:this.props.fontWeight?this.props.fontWeight:'normal'}]}>
            {this.props.children}
        </Text>
      );
    }
  }

  export class HSm extends Component {
    constructor(props) {
      super(props);
    }
  
    render() {
      return (
        <Text style={[styles.hsm,{color:this.props.color?this.props.color:DEFAULT_TEXT_COLOR,fontSize:this.props.fontSize?this.props.fontSize:HSM_SIZE,fontFamily:this.props.fontFamily?this.props.fontFamily:DEFAULT_FONT,textAlign:this.props.textAlign?this.props.textAlign:'center',fontWeight:this.props.fontWeight?this.props.fontWeight:'normal'}]}>
            {this.props.children}
        </Text>
      );
    }
  }

  export class PLg extends Component {
    constructor(props) {
      super(props);
    }
  
    render() {
      return (
        <Text style={[styles.plg,{color:this.props.color?this.props.color:DEFAULT_TEXT_COLOR,fontSize:this.props.fontSize?this.props.fontSize:PLG_SIZE,fontFamily:this.props.fontFamily?this.props.fontFamily:DEFAULT_FONT,textAlign:this.props.textAlign?this.props.textAlign:'center',fontWeight:this.props.fontWeight?this.props.fontWeight:'normal'}]}>
            {this.props.children}
        </Text>
      );
    }
  }

  export class PReg extends Component {
    constructor(props) {
      super(props);
    }
  
    render() {
      return (
        <Text style={[styles.preg,{color:this.props.color?this.props.color:DEFAULT_TEXT_COLOR,fontSize:this.props.fontSize?this.props.fontSize:PREG_SIZE,fontFamily:this.props.fontFamily?this.props.fontFamily:DEFAULT_FONT,textAlign:this.props.textAlign?this.props.textAlign:'center',fontWeight:this.props.fontWeight?this.props.fontWeight:'normal'}]}>
            {this.props.children}
        </Text>
      );
    }
  }

  export class PSm extends Component {
    constructor(props) {
      super(props);
    }
  
    render() {
      return (
        <Text style={[styles.psm,{color:this.props.color?this.props.color:DEFAULT_TEXT_COLOR,fontSize:this.props.fontSize?this.props.fontSize:PSM_SIZE,fontFamily:this.props.fontFamily?this.props.fontFamily:DEFAULT_FONT,textAlign:this.props.textAlign?this.props.textAlign:'center',fontWeight:this.props.fontWeight?this.props.fontWeight:'normal',textDecorationLine:this.props.textDecorationLine}]}>
            {this.props.children}
        </Text>
      );
    }
  }



