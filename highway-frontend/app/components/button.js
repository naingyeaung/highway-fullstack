import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Image } from 'react-native';
import { PRIMARY_COLOR, PREG_SIZE, DEFAULT_TEXT_COLOR, PRIMARY_TEXT_COLOR } from './common';

export class Button extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render() {
        return (
            <TouchableOpacity onPress={this.props.onPress}>
                <View style={{ backgroundColor: PRIMARY_COLOR, height: this.props.height ? this.props.height : 50, justifyContent: 'center', alignContent: 'center' }}>
                    <Text style={{ fontSize: this.props.fontSize ? this.props.fontSize : PREG_SIZE, color: this.props.color ? this.props.color : PRIMARY_TEXT_COLOR, textAlign: 'center' }}>{this.props.text}</Text>
                </View>
            </TouchableOpacity>
        );
    }
}

export class FixedButtonWithIcon extends Component {
    constructor(props) {
        super(props);
        this.state = {

        }
    }

    render() {
        return (
            <TouchableOpacity onPress={this.props.onPress}>
                <View style={{ backgroundColor: PRIMARY_COLOR, height: this.props.height ? this.props.height : 50, justifyContent: 'center', alignContent: 'center', flexDirection:'row' }}>
                    <View style={{ flex: 1, justifyContent:'center' }}>
                        <Text style={{ fontSize: this.props.fontSize ? this.props.fontSize : PREG_SIZE, color: this.props.color ? this.props.color : PRIMARY_TEXT_COLOR, textAlign: 'center', fontWeight:'bold' }}>{this.props.text}</Text>
                    </View>
                    <View style={{ flex: 1, justifyContent: 'center',paddingRight:10 }}>
                        <Image style={{width:25,height:25, alignSelf: 'flex-end'}} source={this.props.img}></Image>
                    </View>

                </View>
            </TouchableOpacity>
        )
    }
}