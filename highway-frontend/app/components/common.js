export const BACKGROUND_COLOR = '#fbfcfe'
export const PRIMARY_COLOR = '#20425B'
export const SECONDARY_COLOR ='#366E96'
export const DEFAULT_TEXT_COLOR = 'black'
export const PRIMARY_TEXT_COLOR = 'white'
export const DEFAULT_FONT = 'Roboto'

export const HLG_SIZE = 25;
export const HREG_SIZE = 22;
export const HSM_SIZE = 20;
export const PLG_SIZE = 18;
export const PREG_SIZE = 15;
export const PSM_SIZE = 13;

export const API_URL = 'http://naingyeaung26-001-site1.ctempurl.com/'