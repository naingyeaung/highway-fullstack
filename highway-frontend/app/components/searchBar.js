import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Image, TextInput } from 'react-native';
import { PRIMARY_COLOR, SECONDARY_COLOR } from './common';
import CityStore from '../mobx/CityStore';
import { observer } from 'mobx-react';

class SearchBar extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    async changeInput(value){
        CityStore.updateSearchContext(value);
        await CityStore.load();
    }

    render() {
        return (
            <View style={{ flexDirection: 'row', backgroundColor: 'white', borderRadius: 15, elevation: 10, height: 40, width: '100%' }}>
                <TouchableOpacity style={{ width: '15%' }}>
                    <View style={{ backgroundColor: 'white', height: 40, justifyContent: 'center', alignItems: 'center', borderRadius: 15 }}>
                        <Image source={require('../assets/images/search.png')} style={{ height: 20, width: 20 }}></Image>
                    </View>
                </TouchableOpacity>
                <TextInput onChangeText={(value)=>this.changeInput(value)} value={CityStore.SearchContext} id='search-text' placeholder='Search Location' style={{ backgroundColor: 'white', width: '86%', height: 40, borderRadius: 15 }}></TextInput>
            </View>
        );
    }
}

export default observer(SearchBar);
