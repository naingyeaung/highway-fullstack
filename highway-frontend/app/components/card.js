import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity } from 'react-native';
import { BACKGROUND_COLOR, DEFAULT_TEXT_COLOR, PRIMARY_COLOR, SECONDARY_COLOR } from './common';
import { HReg, PLg, HSm, PReg, PSm } from './text';
import Br from './br';
import Swipeout from 'react-native-swipeout';
import AsyncStorage from '@react-native-community/async-storage';
import NoteStore from '../mobx/NoteStore';
import FavoriteStore from '../mobx/FavoriteStore';

export class CarCard extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
      <TouchableOpacity onPress={this.props.onPress}>
        <View style={{ backgroundColor: BACKGROUND_COLOR, height: 200, justifyContent: 'center', width: '100%' }}>
          <Image style={{ width: '90%', height: '90%', borderRadius: 15 }} source={this.props.img}>

          </Image>
          <HSm textAlign='left' color={PRIMARY_COLOR} fontWeight='bold'>{this.props.title}</HSm>
        </View>
      </TouchableOpacity>
    );
  }
}

export class RoundedImageCard extends Component {
  constructor(props) {
    super(props);
    this.state = {

    };
  }

  render() {
    return (
      <View style={{ width: 100, height: 100, borderRadius: 50 }}>
        <Image style={{ width: '100%', height: '100%', borderRadius: 50 }} source={this.props.img}>

        </Image>
      </View>
    )
  }
}

export class ReviewCard extends Component {
  constructor(props) {
    super(props);
    this.state = {

    };
  }

  render() {
    return (
      <View style={{ width: '100%', borderRadius: 10, elevation: 5, flexDirection: 'row', padding: 10, backgroundColor: 'white' }}>
        <View style={{ flex: 1, justifyContent: 'center' }}>
          <Image style={{ width: 25, height: 25, alignSelf: 'center' }} source={require('../assets/images/review-icon.png')}></Image>
        </View>
        <View style={{ flex: 5 }}>
          <PReg fontWeight='bold' color={PRIMARY_COLOR} textAlign='left'>{this.props.date}</PReg>
          <PReg textAlign='left'>{this.props.review}</PReg>
        </View>
        <View style={{ flex: 1, justifyContent: 'center' }}>
          <TouchableOpacity onPress={() => this.props.navigation.navigate('Feedback')}>
            <Image style={{ width: 25, height: 25, alignSelf: 'center' }} source={require('../assets/images/view-icon.png')}></Image>
          </TouchableOpacity>
        </View>
      </View>
    )
  }
}

export class NoteCard extends Component {
  constructor(props) {
    super(props);
    this.state = {

    }
    this.deleteNote = this.deleteNote.bind(this);
  }

  async deleteNote() {
    var result = await AsyncStorage.getItem('notes');
    var notes = JSON.parse(result);
    var note = notes.filter(x=>x.id==this.props.id);
    if(note.length>0){
      var index = notes.indexOf(note[0]);
      if(index>=0){
        console.log(index)
        notes.splice(index,1);
        await AsyncStorage.setItem('notes',JSON.stringify(notes));
        await NoteStore.load();
      }
    }
  }

  render() {
    var swipeoutBtns = [
      {
        text: 'Remove',
        backgroundColor: 'red',
        color: 'white',
        onPress: this.deleteNote
      }
    ]
    return (
      <Swipeout right={swipeoutBtns} backgroundColor={PRIMARY_COLOR}>
        <View style={{ backgroundColor: this.props.backgroundColor ? this.props.backgroundColor : PRIMARY_COLOR, padding: 20, borderRadius: 10, elevation: 5 }}>
          <View style={{ flexDirection: 'row' }}>
            <View style={{ flex: 1 }}>
              <HSm color='white' textAlign='left' fontWeight='bold'>{this.props.title}</HSm>
            </View>
            <View style={{ flex: 1 }}>
              <PReg color='white' textAlign='right' fontWeight='bold'>27 Aug, 2019</PReg>
            </View>
          </View>
          <Br height={15}></Br>
          <PReg color='white' textAlign='left'>{this.props.note}</PReg>
          <Br height={5}></Br>

        </View>
      </Swipeout>
    )
  }
}

export class FavouriteCard extends Component {
  constructor(props) {
    super(props);
    this.state = {

    }
    this.deleteFavorite = this.deleteFavorite.bind(this);
  }

  async deleteFavorite() {
    var result = await AsyncStorage.getItem('favorites');
    var favorites = JSON.parse(result);
    var favorite = favorites.filter(x=>x.id==this.props.id);
    if(favorite.length>0){
      var index = favorites.indexOf(favorite[0]);
      if(index>=0){
        console.log(index)
        favorites.splice(index,1);
        await AsyncStorage.setItem('favorites',JSON.stringify(favorites));
        await FavoriteStore.load();
      }
    }
  }

  render() {
    var swipeoutBtns = [
      {
        text: 'Remove',
        backgroundColor: 'red',
        color: 'white',
        onPress:this.deleteFavorite
      }
    ]
    return (
      <Swipeout right={swipeoutBtns} backgroundColor='white'>
        <View style={{ flexDirection: 'row', elevation: 5, flex: 1 }}>
          <View style={{ flex: 1, padding: 20 }}>
            <TouchableOpacity onPress={() => this.props.navigation.navigate('BusDetail', { busId: this.props.id })}>
              <Image style={{ width: '100%', height: '100%' }} source={this.props.img}></Image>
            </TouchableOpacity>
          </View>
          <View style={{ flex: 1, padding: 20 }}>
            <HReg fontWeight='bold' textAlign='left'>{this.props.title}</HReg>
            <Br height={10}></Br>
            <PReg textAlign='left'>{this.props.description}</PReg>
          </View>
        </View>
      </Swipeout>
    )
  }
}

export class FeedbackCard extends Component {
  constructor(props) {
    super(props);
    this.state = {

    }
  }

  render() {
    return (
      <View style={{ flex: 1, flexDirection: 'row', borderBottomColor: '#DCDCDC', borderBottomWidth: 0.8, padding: 10 }}>
        <View style={{ flex: 1 }}>
          <Image style={{ width: 50, height: 50 }} source={require('../assets/images/profile.png')}></Image>
        </View>
        <View style={{ flex: 3 }}>
          <HReg fontWeight='bold' textAlign='left'>{this.props.name}</HReg>
          <Br height={10}></Br>
          <PReg textAlign='left'>{this.props.description}</PReg>
          <Br height={5}></Br>
          <PSm color='#666' textAlign='left'>{this.props.date}</PSm>
        </View>
      </View>
    )
  }
}

export class BusCard extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
      <TouchableOpacity onPress={this.props.onPress}>
        <View style={{ backgroundColor: BACKGROUND_COLOR, height: 240, justifyContent: 'center', width: '100%' }}>
          <Image style={{ width: '100%', height: '80%' }} source={this.props.img}>

          </Image>
          <HSm textAlign='left' color={PRIMARY_COLOR} fontWeight='bold'>{this.props.title}</HSm>
          <PReg textAlign='left'>{this.props.description}</PReg>
        </View>
      </TouchableOpacity>
    );
  }
}