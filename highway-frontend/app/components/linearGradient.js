import React, { Component } from 'react'
import { View } from 'react-native'
import { SECONDARY_COLOR, PRIMARY_TEXT_COLOR, PRIMARY_COLOR } from './common';
import LinearGradient from 'react-native-linear-gradient';

export default class LinearGradientBg extends Component {
    render() {
        return (
            <LinearGradient
                start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }}
                colors={[SECONDARY_COLOR, PRIMARY_COLOR, PRIMARY_COLOR]}
                style={this.props.style}
            >
                {this.props.children}
            </LinearGradient>
        )
    }
}

export class FacebookLinearGradientBg extends Component {
    render() {
        return (
            <LinearGradient
                start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }}
                colors={['#19aeff', '#0164e1', '#0164e1']}
                style={this.props.style}
            >
                {this.props.children}
            </LinearGradient>
        )
    }
}