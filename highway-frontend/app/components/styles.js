import {HLG_SIZE, DEFAULT_FONT, DEFAULT_TEXT_COLOR, HREG_SIZE, HSM_SIZE, PLG_SIZE, PREG_SIZE, PSM_SIZE, PRIMARY_TEXT_COLOR, SECONDARY_TEXT_COLOR, PRIMARY_COLOR, BACKGROUND_COLOR} from './common'
import {StyleSheet} from 'react-native'

export const styles = StyleSheet.create({
    hlg:{
        fontFamily: DEFAULT_FONT,
        fontSize: HLG_SIZE,
        color:DEFAULT_TEXT_COLOR
    },
    hreg:{
        fontFamily:DEFAULT_FONT,
        fontSize:HREG_SIZE,
        color:DEFAULT_TEXT_COLOR
    },
    hsm:{
        fontFamily:DEFAULT_FONT,
        fontSize:HSM_SIZE,
        color:DEFAULT_TEXT_COLOR
    },
    plg:{
        fontFamily:DEFAULT_FONT,
        fontSize:PLG_SIZE,
        color:DEFAULT_TEXT_COLOR
    },
    preg:{
        fontFamily:DEFAULT_FONT,
        fontSize:PREG_SIZE,
        color:DEFAULT_TEXT_COLOR
    },
    psm:{
        fontFamily:DEFAULT_FONT,
        fontSize:PSM_SIZE,
        color:DEFAULT_TEXT_COLOR
    },
})