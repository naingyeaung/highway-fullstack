import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity } from 'react-native';
import { PRIMARY_COLOR, SECONDARY_COLOR } from '../components/common';
import { HReg, PReg } from '../components/text';
import SearchBar from '../components/searchBar';
import Br from '../components/br';
import LinearGradient from 'react-native-linear-gradient';
import LinearGradientBg from '../components/linearGradient';

export class HomeHeader extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
      <LinearGradientBg style={{ height: 200 }}>
        <View style={{ flex: 4, justifyContent: 'center', alignItems: 'center', alignContent: 'center' }}>
          <Image style={{ width: 60, height: 60, alignSelf: 'center' }} source={require('../assets/images/logo.png')}></Image>
          <Br height={10}></Br>
          <HReg color='white' fontWeight='bold' textAlign='center'>Highway Directory</HReg>
        </View>
        <View style={{ flex: 1, padding: 30 }}>
          <SearchBar></SearchBar>
        </View>

      </LinearGradientBg>

    );
  }
}


export class Header extends Component {
  render() {
    return (
      <LinearGradientBg style={{ height: 50, justifyContent: 'center' }}>
        <HReg color='white' fontWeight='bold'>{this.props.title}</HReg>
      </LinearGradientBg>
    )
  }
}

export class HeaderBack extends Component {
  constructor(props){
    super(props);
    this.state={

    }
  };

  render() {
    const { navigation } = this.props;
    return (
      <LinearGradientBg style={{ height: 50, justifyContent: 'center', flexDirection: 'row', }}>
        <View style={{ flex: 1, justifyContent: 'center', paddingLeft: 5 }}>
          <TouchableOpacity onPress={()=>navigation.goBack(null)}>
            <Image style={{ width: 15, height: 15 }} source={require('../assets/images/back.jpg')} resizeMode='contain'></Image>
          </TouchableOpacity>
        </View>
        <View style={{ flex: 1, justifyContent: 'center' }}>
          <HReg color='white' fontWeight='bold'>{this.props.title}</HReg>
        </View>
        <View style={{ flex: 1 }}></View>

      </LinearGradientBg>
    )
  }
}
