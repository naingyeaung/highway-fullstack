import React, { Component } from 'react'
import { Image, View } from 'react-native'
import { createStackNavigator, createAppContainer, createBottomTabNavigator, createSwitchNavigator } from 'react-navigation'
import Home from '../views/home';
import { HomeHeader, Header, HeaderBack } from './header';
import Favourite from '../views/favourite';
import Feedback from '../views/feedback';
import { PSm } from '../components/text';
import { PRIMARY_COLOR } from '../components/common';
import Note from '../views/note';
import Bus from '../views/bus';
import BusDetail from '../views/busDetail';
import AddFeedback from '../views/addFeedback';
import AddNote from '../views/addNote';

const HomeStack = createStackNavigator({
    Home: {
        screen: Home,
        navigationOptions: {
            header: (({ navigation }) => <HomeHeader navigation={navigation}></HomeHeader>)
        }
    },
    Bus:{
        screen:Bus,
        navigationOptions:{
            header:(({navigation})=>null)
        }
    },
    BusDetail:{
        screen:BusDetail,
        navigationOptions:{
            header:(({navigation})=><HeaderBack navigation={navigation}></HeaderBack>)
        }
    }
});

const FavouriteStack = createStackNavigator({
    Favourite: {
        screen: Favourite,
        navigationOptions: {
            header: (({ navigation }) => <Header title='Favourite' navigation={navigation}></Header>)
        }
    }
})

const FeedbackStack = createStackNavigator({
    Feedback: {
        screen: Feedback,
        navigationOptions: {
            header: (({ navigation }) => <Header title='Feedback' navigation={navigation}></Header>)
        }
    },
    AddFeedback:{
        screen:AddFeedback,
        navigationOptions:{
            header:(({navigation})=><HeaderBack title='Give Feedback' navigation={navigation}></HeaderBack>)
        }
    }
})

const NoteStack = createStackNavigator({
    Note: {
        screen: Note,
        navigationOptions: {
            header: (({ navigation }) => <Header title="Note" navigation={navigation}></Header>)
        }
    },
    AddNote:{
        screen:AddNote,
        navigationOptions:{
            header:(({navigation})=><HeaderBack title="Add Note" navigation={navigation}></HeaderBack>)
        }
    }
})

const UserStack = createBottomTabNavigator({
    Home: {
        screen: HomeStack,
        navigationOptions: {
            tabBarIcon: ({ focused, tintColor }) => (
                <TabBarIcon focused={focused} focusedIcon={require('../assets/images/home.png')} icon={require('../assets/images/home.png')}></TabBarIcon>
            ),
            tabBarLabel: ({ focused }) => (
                <View style={{ marginTop: -10, justifyContent: 'center', alignItems: 'center' }}>
                    <PSm color={focused ? PRIMARY_COLOR : 'black'} fontWeight='bold'>
                        {"Home"}
                    </PSm>
                </View>
            )
        }
    },
    Favourite: {
        screen: FavouriteStack,
        navigationOptions: {
            tabBarIcon: ({ focused, tintColor }) => (
                <TabBarIcon focused={focused} focusedIcon={require('../assets/images/favourite.png')} icon={require('../assets/images/favourite.png')}></TabBarIcon>
            ),
            tabBarLabel: ({ focused }) => (
                <View style={{ marginTop: -10, justifyContent: 'center', alignItems: 'center' }}>
                    <PSm color={focused ? PRIMARY_COLOR : 'black'} fontWeight='bold'>
                        {"Favourite"}
                    </PSm>
                </View>
            )
        }
    },
    Feedback: {
        screen: FeedbackStack,
        navigationOptions: {
            tabBarIcon: ({ focused, tintColor }) => (
                <TabBarIcon focused={focused} focusedIcon={require('../assets/images/feedback.png')} icon={require('../assets/images/feedback.png')}></TabBarIcon>
            ),
            tabBarLabel: ({ focused }) => (
                <View style={{ marginTop: -10, justifyContent: 'center', alignItems: 'center' }}>
                    <PSm color={focused ? PRIMARY_COLOR : 'black'} fontWeight='bold'>
                        {"Feedback"}
                    </PSm>
                </View>
            )
        }
    },
    Note: {
        screen: NoteStack,
        navigationOptions: {
            tabBarIcon: ({ focused, tintColor }) => (
                <TabBarIcon focused={focused} focusedIcon={require('../assets/images/note.png')} icon={require('../assets/images/note.png')}></TabBarIcon>
            ),
            tabBarLabel: ({ focused }) => (
                <View style={{ marginTop: -10, justifyContent: 'center', alignItems: 'center' }}>
                    <PSm color={focused ? PRIMARY_COLOR : 'black'} fontWeight='bold'>
                        {"Note"}
                    </PSm>
                </View>
            )
        }
    }
},
    {
        tabBarOptions: {
            activeTintColor: PRIMARY_COLOR,
            inactiveTintColor: 'gray',
            style: { elevation: 10, borderRadius: 0, borderTopWidth: 0, height: 60, paddingBottom: 10, position: 'relative' },

        },
    }
);

const Root = createSwitchNavigator({
    User: {
        screen: UserStack,
        navigationOptions: {
            header: null
        }
    }
})

export default createAppContainer(Root);

class TabBarIcon extends Component {
    render() {
        return (
            <Image source={this.props.focused ? this.props.focusedIcon : this.props.icon} style={{ width: 25, height: 25, resizeMode: 'contain' }}></Image>
        )
    }
}