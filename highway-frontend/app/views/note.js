import React, { Component } from 'react';
import { View, Text, ScrollView } from 'react-native';
import { NoteCard } from '../components/card';
import { Button, FixedButtonWithIcon } from '../components/button';
import Br from '../components/br';
import { PLG_SIZE, PRIMARY_COLOR } from '../components/common';
import NoteStore from '../mobx/NoteStore';
import { observer } from 'mobx-react';

class Note extends Component {
  constructor(props) {
    super(props);
    this.state = {
      notes: NoteStore.Notes
    };
  }

  async componentDidMount() {
    await NoteStore.load();
    console.log(NoteStore.Notes.toJS())
    console.log(this.state.notes.length)
  }

  render() {
    return (
      <View style={{ flex: 1 }}>
        <View style={{ flex: 8 }}>
          <ScrollView style={{ padding: 10 }}>
            {
              NoteStore.Notes.map((note, index) => (
                <View key={index}>
                  <NoteCard id={note.id} backgroundColor={PRIMARY_COLOR} title={note.title} note={note.description}></NoteCard>
                  <Br height={15}></Br>
                </View>
              ))
            }
          </ScrollView>
        </View>
        <View style={{ flex: 1, justifyContent: 'flex-end' }}>
          <FixedButtonWithIcon text='Add New Note' img={require('../assets/images/plus.png')} fontSize={PLG_SIZE} onPress={() => this.props.navigation.navigate('AddNote')}></FixedButtonWithIcon>
        </View>
      </View>

    );
  }
}

export default observer(Note);
