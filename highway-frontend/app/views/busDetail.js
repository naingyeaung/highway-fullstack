import React, { Component } from 'react';
import { View, Text, Image, ScrollView, TouchableOpacity } from 'react-native';
import LinearGradientBg from '../components/linearGradient';
import { HReg, PSm, HSm, PReg, PLg } from '../components/text';
import Br from '../components/br';
import AsyncStorage from '@react-native-community/async-storage';
import { getData } from '../services/fetch';
import { API_URL } from '../components/common';
import FavoriteStore from '../mobx/FavoriteStore';

class BusDetail extends Component {
    constructor(props) {
        super(props);
        const { navigation } = this.props;
        this.state = {
            busId: navigation.getParam('busId'),
            bus: { cities: [], bus: {} }
        };
    }

    async componentDidMount() {
        var response = await getData(API_URL + 'api/bus/detail?id=' + this.state.busId);
        if (response.ok) {
            var bus = response.data;
            this.setState({ bus: bus });
        } else {
            console.log(response.data);
        }
    }

    async addToFavorite() {
        var responses = await AsyncStorage.getItem('favorites');
        var buses = JSON.parse(responses);
        if (buses != null && buses.length > 0) {
            var same = buses.filter(x => x.id == this.state.bus.bus.id);
            if (same.length > 0) {

            } else {
                
                var favBus = { id: this.state.bus.bus.id, name: this.state.bus.bus.name, description: this.state.bus.bus.description, photo: this.state.bus.bus.photo }
                buses.push(favBus);
            }
        } else {
            buses = new Array();
            var favBus = { id: this.state.bus.bus.id, name: this.state.bus.bus.name, description: this.state.bus.bus.description, photo: this.state.bus.bus.photo }
            buses.push(favBus);
        }
        await AsyncStorage.setItem('favorites', JSON.stringify(buses));
        await FavoriteStore.load();

        this.props.navigation.navigate('Favourite')
    }

    render() {
        const bus = this.state.bus;
        return (
            <View style={{ flex: 1 }}>
                <LinearGradientBg style={{ height: 150 }}>
                    <View style={{ height: 150, padding: 15, justifyContent: 'flex-end', paddingBottom: 0 }}>
                        <View style={{ flex: 1, justifyContent: 'center' }}>
                            <HReg color='white' fontWeight='bold' textAlign='left'>{bus.bus.name}</HReg>
                            <Br height={10}></Br>
                            <PSm color='white' fontWeight='bold' textAlign='left'>Transporting through {bus.cities.length} cities</PSm>
                        </View>
                        <View style={{ alignSelf: 'flex-end', justifyContent: 'flex-end', flex: 1, marginBottom: -25 }}>
                            <TouchableOpacity onPress={()=>this.addToFavorite()}>
                                <Image style={{ width: 50, height: 50 }} source={require('../assets/images/white-heart.png')} resizeMode='contain'></Image>
                            </TouchableOpacity>
                        </View>
                    </View>
                </LinearGradientBg>
                <ScrollView style={{ padding: 15 }}>
                    <View>
                        <HReg fontWeight='bold' textAlign='left'>Description</HReg>
                        <Br height={10}></Br>
                        <PReg fontWeight='bold' textAlign='left'>{bus.bus.description}</PReg>
                    </View>
                    <Br height={15}></Br>
                    <View>
                        <HReg fontWeight='bold' textAlign='left'>Photos</HReg>
                        <Br height={10}></Br>
                        <Image style={{ width: '100%', height: 200 }} source={{ uri: bus.bus.photo }}></Image>
                    </View>
                    <Br height={15}></Br>
                    <View>
                        <HReg fontWeight='bold' textAlign='left'>Avaliable Cities</HReg>
                        <Br height={10}></Br>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-evenly' }}>
                            {
                                bus.cities.map((city, index) =>
                                    <PReg key={index} fontWeight='bold' textAlign='left'>{city.name}</PReg>
                                )
                            }
                            {/* <View style={{ flex: 1}}>
                                <PReg fontWeight='bold' textAlign='left'>Yangon</PReg>
                                <PReg fontWeight='bold' textAlign='left'>Mandalay</PReg>
                                <PReg fontWeight='bold' textAlign='left'>Naypyitaw</PReg>
                                <PReg fontWeight='bold' textAlign='left'>Bagan</PReg>
                                <PReg fontWeight='bold' textAlign='left'>Mawlamyaing</PReg>
                                <PReg fontWeight='bold' textAlign='left'>Pyin Oo Lwin</PReg>
                            </View>
                            <View style={{ flex: 1}}>
                                <PReg fontWeight='bold' textAlign='left'>Myawady</PReg>
                                <PReg fontWeight='bold' textAlign='left'>Kaw Thaung</PReg>
                                <PReg fontWeight='bold' textAlign='left'>Myitkyina</PReg>
                                <PReg fontWeight='bold' textAlign='left'>Taungyi</PReg>
                                <PReg fontWeight='bold' textAlign='left'>Innlay</PReg>
                                <PReg fontWeight='bold' textAlign='left'>Lasho</PReg>
                            </View> */}
                        </View>
                    </View>
                    <Br height={20}></Br>
                </ScrollView>
            </View>
        );
    }
}

export default BusDetail;
