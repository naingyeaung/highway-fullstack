import React, { Component } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { HomeHeader } from '../navigation/header';
import { ScrollView } from 'react-native-gesture-handler';
import { HSm, HReg, PReg } from '../components/text';
import { CarCard, RoundedImageCard, ReviewCard } from '../components/card';
import { getData } from '../services/fetch';
import { API_URL, PRIMARY_COLOR, SECONDARY_COLOR } from '../components/common';
import SearchBar from '../components/searchBar';
import Br from '../components/br';
import CityStore from '../mobx/CityStore';
import BusStore from '../mobx/BusStore';
import FeedbackStore from '../mobx/FeedbackStore';
import { observer } from 'mobx-react';

class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {
            cities: [],
            buses: [],
            feedbacks: []
        };
    }

    async componentDidMount() {
        await CityStore.load();
        await BusStore.load();
        // await FeedbackStore.load();

        var response = await getData(API_URL+'api/bus/getPopularBuses');
        if(response.ok){
            var buses = response.data;
            this.setState({buses:buses});
        }

        var fResponse = await getData(API_URL+'api/feedback/getPopularFeedbacks');
        if(response.ok){
            var feedbacks = fResponse.data;
            this.setState({feedbacks:feedbacks});
        }
    }

    async clearSearchCity(){
        CityStore.clearSearch();
        this.props.navigation.navigate('Bus')
    }


    render() {
        return (
            <View style={{ flex: 1, backgroundColor: 'white' }}>
                <ScrollView>
                    <View style={{ padding: 10 }}>
                        <HReg color={PRIMARY_COLOR} fontWeight='bold' textAlign='left'>Places</HReg>
                        <Br height={20}></Br>
                        <ScrollView horizontal={true} style={{ height: 150 }} showsHorizontalScrollIndicator={false}>
                            {
                                CityStore.Cities.map((city, index) =>
                                    <PlaceCard img={{ uri: city.url }} name={city.name} key={index} id={city.id} navigation={this.props.navigation}>

                                    </PlaceCard>
                                )
                            }

                            {/* <PlaceCard img={{ uri: 'https://lonelyplanetimages.imgix.net/mastheads/GettyImages-184322781_medium.jpg?sharp=10&vib=20&w=1200' }} name='Mandalay'>

                            </PlaceCard>
                            <PlaceCard img={{ uri: 'http://www.myanmartours.net/userfiles/images/botanical-garden.jpg' }} name='Pyin Oo Lwin'>

                            </PlaceCard>
                            <PlaceCard img={{ uri: 'https://www.tripsavvy.com/thmb/gOcOe0MyJU8lgY-Zic8kdUoEBuA=/960x0/filters:no_upscale():max_bytes(150000):strip_icc()/GettyImages-646927586-5af207593037130036dbf349.jpg' }} name='Bagan'>

                            </PlaceCard> */}
                        </ScrollView>
                        <Br height={10}></Br>
                        <View style={{ flexDirection: 'row' }}>
                            <View style={{ flex: 1 }}>
                                <HReg color={PRIMARY_COLOR} fontWeight='bold' textAlign='left'>Popular Buses</HReg>
                            </View>
                            <View style={{ flex: 1 }}>
                                <TouchableOpacity onPress={() => this.clearSearchCity()}>
                                    <PReg color={SECONDARY_COLOR} textAlign='right' fontWeight='bold'>View All</PReg>
                                </TouchableOpacity>
                            </View>
                        </View>
                        <Br height={20}></Br>
                        <ScrollView horizontal={true} style={{ height: 200 }} showsHorizontalScrollIndicator={false}>
                            {
                                this.state.buses.map((bus, index) =>
                                    <View style={{ width: 170 }} key={index}>
                                        <CarCard img={{ uri: bus.photo }} title={bus.name} onPress={() => this.props.navigation.navigate('BusDetail', { busId: bus.id })}></CarCard>
                                    </View>
                                )
                            }

                            {/* <View style={{ width: 170 }}>
                                <CarCard img={{ uri: 'https://easycdn.blob.core.windows.net/operators/operator_00088_190524092243184.png' }} title='Elite Myanmar'></CarCard>
                            </View>
                            <View style={{ width: 170 }}>
                                <CarCard img={{ uri: 'http://ssskexpress.com/uploads/md_1518544776.jpg' }} title='Shwe Sin SetKyar'></CarCard>
                            </View> */}
                        </ScrollView>
                        <Br height={30}></Br>
                        <View style={{ flexDirection: 'row' }}>
                            <View style={{ flex: 1 }}>
                                <HReg color={PRIMARY_COLOR} fontWeight='bold' textAlign='left'>Recent Feedbacks</HReg>
                            </View>
                            <View style={{ flex: 1 }}>
                                <TouchableOpacity onPress={() => this.props.navigation.navigate('Feedback')}>
                                    <PReg color={SECONDARY_COLOR} textAlign='right' fontWeight='bold'>View All</PReg>
                                </TouchableOpacity>
                            </View>
                        </View>
                        <View style={{ paddingTop: 20 }}>
                            {
                                this.state.feedbacks.map((feedback, index) =>
                                    <View key={index}>
                                        <ReviewCard navigation={this.props.navigation} review={feedback.description.length > 20 ? feedback.description.slice(0, 20) : feedback.description} date={feedback.createdOn}></ReviewCard>
                                        <Br height={15}></Br>
                                    </View>
                                )
                            }

                            {/* <ReviewCard review='This is a really good app with various routes and informations an...' date='Aug 2, 2019'></ReviewCard> */}
                        </View>
                    </View>
                </ScrollView>
            </View>
        );
    }
}

export default observer(Home);


class PlaceCard extends Component {

    async changeCurrentCity(){
        CityStore.updateCurrentCity(this.props.id, this.props.name);
        this.props.navigation.navigate('Bus')
    }

    render() {
        return (
            <TouchableOpacity onPress={()=>this.changeCurrentCity()}>
                <View style={{ width: 120, height: 200, alignItems: 'center' }}>
                    <RoundedImageCard img={this.props.img}>

                    </RoundedImageCard>
                    <HSm fontWeight='bold' color={PRIMARY_COLOR} textAlign='center'>{this.props.name}</HSm>
                </View>
            </TouchableOpacity>
        )
    }
}