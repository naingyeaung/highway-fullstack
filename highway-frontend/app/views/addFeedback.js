import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { TextField } from 'react-native-material-textfield';
import Br from '../components/br';
import { Button } from '../components/button';
import { postData } from '../services/fetch';
import { API_URL } from '../components/common';
import FeedbackStore from '../mobx/FeedbackStore';

class AddFeedback extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name : '',
      feedback : ''
    };
  }

  async postFeedback(){
    const model = {
      name : this.state.name,
      description : this.state.feedback
    }
    var response = await postData(API_URL+'api/feedback/create', model);
    if(response.ok){
      await FeedbackStore.load();
      this.props.navigation.navigate('Feedback')
    }else{
      console.log(response.data)
    }
  }

  render() {
    return (
      <View style={{flex:1,padding:15}}>
        <TextField label='Name(optional)' value={this.state.name} onChangeText={(value)=>this.setState({name:value})}></TextField>
        <TextField label='Feedback' multiline={true} value={this.state.feedback} onChangeText={(value)=>this.setState({feedback:value})}></TextField>
        <Br height={20}></Br>
        <Button text='Submit Feedback' onPress={()=>this.postFeedback()}></Button>
      </View>
    );
  }
}

export default AddFeedback;
