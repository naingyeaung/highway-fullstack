import React, { Component } from 'react';
import { View, Text, ScrollView, TouchableOpacity } from 'react-native';
import { FavouriteCard } from '../components/card';
import { BACKGROUND_COLOR } from '../components/common';
import Br from '../components/br';
import FavoriteStore from '../mobx/FavoriteStore';
import { observer } from 'mobx-react';

class Favourite extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  async componentDidMount() {
    await FavoriteStore.load();
  }

  render() {
    return (
      <View style={{ backgroundColor: BACKGROUND_COLOR, flex: 1 }}>
        <ScrollView style={{ padding: 15, flex: 1 }}>
          {
            FavoriteStore.Favorites.map((favorite, index) =>
              <View key={index}>
                {/* <TouchableOpacity onPress={()=>this.props.navigation.navigate('BusDetail',{busId:favorite.id})}> */}
                  <FavouriteCard id={favorite.id} navigation={this.props.navigation} img={{ uri: favorite.photo }} title={favorite.name} description={favorite.description.length > 50 ? favorite.description.slice(0, 50) : favorite.description}></FavouriteCard>
                {/* </TouchableOpacity> */}
                <Br height={15}></Br>
              </View>)
          }

          {/* <FavouriteCard img={{ uri: 'https://easycdn.blob.core.windows.net/operators/operator_00088_190524092243184.png' }} title='Elite' description='One of the best expresses in Myanmar for travelling'></FavouriteCard> */}
        </ScrollView>
      </View>
    );
  }
}

export default observer(Favourite);
