import React, { Component } from 'react';
import { View, Text, ScrollView, TextInput, TouchableOpacity, Image } from 'react-native';
import { BusCard } from '../components/card';
import Br from '../components/br';
import { PRIMARY_COLOR } from '../components/common';
import { HeaderBack } from '../navigation/header';
import CityStore from '../mobx/CityStore';
import BusStore from '../mobx/BusStore';
import {NavigationEvents} from 'react-navigation';
import { observer } from 'mobx-react';

class Bus extends Component {
    constructor(props) {
        super(props);
        this.state = {

        };
    }

    async loadBus(){
        await BusStore.load();
    }


    render() {
        return (
            <View style={{ flex: 1 }}>
                <NavigationEvents onDidFocus={() => this.loadBus()} />
                <HeaderBack title={CityStore.CurrentCityName == '' ? 'Buses' : CityStore.CurrentCityName} navigation={this.props.navigation}></HeaderBack>
                <View style={{ flexDirection: 'row', padding: 10 }}>
                    <TextInput value={BusStore.SearchContex} onChangeText={(value)=>BusStore.updateSearchContext(value)} style={{ borderColor: PRIMARY_COLOR, borderWidth: 1, borderRadius: 5, height: 40, width: '85%', borderBottomRightRadius: 0, borderTopRightRadius: 0 }} placeholder='Search Buses'></TextInput>
                    <TouchableOpacity style={{ width: '15%' }} onPress={()=>this.loadBus()}>
                        <View style={{ backgroundColor: PRIMARY_COLOR, width: '100%', height: 40, borderTopRightRadius: 5, borderBottomRightRadius: 5, justifyContent: 'center', alignContent: 'center' }}>
                            <Image style={{ width: '60%', height: '60%', alignSelf: 'center' }} source={require('../assets/images/search-white.png')} resizeMode='contain'></Image>
                        </View>
                    </TouchableOpacity>
                </View>
                <ScrollView>
                    <View style={{ padding: 10 }}>
                        {
                            BusStore.Buses.map((bus, index) =>
                                <View key={index}>
                                    <BusCard img={{ uri: bus.photo }} title={bus.name} description={bus.description.length>20?bus.description.slice(0,20)+'...':bus.description} onPress={() => this.props.navigation.navigate('BusDetail',{busId:bus.id})}></BusCard>
                                    <Br height={15}></Br>
                                </View>)
                        }

                        {/* <BusCard img={{ uri: 'https://easycdn.blob.core.windows.net/operators/operator_00088_190524092243184.png' }} title='Elite Myanmar' description='Established from 1998, Shwe Mandalar is one of the oldest...'></BusCard>
                        <Br height={15}></Br>
                        <BusCard img={{ uri: 'http://ssskexpress.com/uploads/md_1518544776.jpg' }} title='Shwe Sin SetKyar' description='Established from 1998, Shwe Mandalar is one of the oldest...'></BusCard>
                        <Br height={15}></Br> */}
                    </View>
                </ScrollView>
            </View>
        );
    }
}

export default observer(Bus);
