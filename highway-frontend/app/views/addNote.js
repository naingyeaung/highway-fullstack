import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { TextField } from 'react-native-material-textfield';
import Br from '../components/br';
import { Button } from '../components/button';
import AsyncStorage from '@react-native-community/async-storage';
import NoteStore from '../mobx/NoteStore';

class AddNote extends Component {
    constructor(props) {
        super(props);
        this.state = {
            title: '',
            description: '',
            notes: []
        };
    }

    async _addNote() {       
        var result = await AsyncStorage.getItem('notes')
        var previousNotes = JSON.parse(result);
        var continueid = previousNotes==null||previousNotes.length==0?0:previousNotes[previousNotes.length-1].id+1
        const model = {
            id: continueid,
            title: this.state.title,
            description: this.state.description
        }
        if (previousNotes != null) {
            previousNotes.push(model);
            await AsyncStorage.setItem('notes',JSON.stringify(previousNotes));

        } else {
            this.state.notes.push(model);
            this.setState({notes:this.state.notes});
            await AsyncStorage.setItem('notes',JSON.stringify(this.state.notes));
        }
        await NoteStore.load();
        this.props.navigation.navigate('Note')
    }

    render() {
        return (
            <View style={{ flex: 1, padding: 15 }}>
                <TextField label='Title' onChangeText={(value)=>this.setState({title:value})} value={this.state.title}></TextField>
                <TextField label='Note' multiline={true} onChangeText={(value)=>this.setState({description:value})} value={this.state.description}></TextField>
                <Br height={20}></Br>
                <Button text='Add Note' onPress={()=>this._addNote()}></Button>
            </View>
        );
    }
}

export default AddNote;
