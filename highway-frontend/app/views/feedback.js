import React, { Component } from 'react';
import { View, Text, ScrollView } from 'react-native';
import { FeedbackCard } from '../components/card';
import { FixedButtonWithIcon } from '../components/button';
import { PLG_SIZE } from '../components/common';
import FeedbackStore from '../mobx/FeedbackStore';
import { observer } from 'mobx-react';

class Feedback extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  async componentDidMount(){
    await FeedbackStore.load();
  }

  render() {
    return (
      <View style={{ flex: 1 }}>
        <View style={{ flex: 8 }}>
          <ScrollView style={{ padding: 15 }}>
            {
              FeedbackStore.Feedbacks.map((feedback, index) =>
                <FeedbackCard key={index} name={feedback.name} description={feedback.description} date={feedback.createdOn}></FeedbackCard>)
            }

            {/* <FeedbackCard name='Naing Ye Aung' description='This is one of the best high way apps in Myanmar.' date='Aug 2, 2019'></FeedbackCard> */}
          </ScrollView>
        </View>
        <View style={{ flex: 1, justifyContent: 'flex-end' }}>
          <FixedButtonWithIcon text='Give Feedback' img={require('../assets/images/right-arrow.png')} fontSize={PLG_SIZE} onPress={() => this.props.navigation.navigate('AddFeedback')}></FixedButtonWithIcon>
        </View>
      </View>
    );
  }
}

export default observer(Feedback);
