import { types } from "mobx-state-tree";
import AsyncStorage from "@react-native-community/async-storage";
import { getData } from "../services/fetch";
import { API_URL } from "../components/common";


const City = types.model('City', {
    id: types.string,
    name: types.string,
    url: types.string
})

const CityStore = types.model({
    Cities: types.array(City),
    CurrentCity: types.string,
    CurrentCityName: types.string,
    SearchContext: types.string
})
    .actions(self => ({
        async load() {
            // console.log(API_URL+'api/city/searchCity?search='+self.SearchContext);
            var response = await getData(API_URL + 'api/city/searchCity?search=' + self.SearchContext);
            if (response.ok) {
                var buses = response.data;
                self.updateCities(buses);
                console.log(self.Cities)
            } else {
                console.log(response.data);
            }
        },
        updateCities(citiesToUpdate) {
            self.Cities = citiesToUpdate;
        },
        updateCurrentCity(id, name) {
            self.CurrentCity = id;
            self.CurrentCityName = name;
        },
        updateSearchContext(context) {
            self.SearchContext = context;
        },
        clearSearch() {
            self.CurrentCityName = '';
            self.CurrentCity = '';
        }
    }))
    .create({
        Cities: [],
        CurrentCity: '',
        SearchContext: '',
        CurrentCityName: ''
    })

export default CityStore;