import { types } from "mobx-state-tree";
import AsyncStorage from "@react-native-community/async-storage";
import { getData } from "../services/fetch";
import { API_URL } from "../components/common";

const feedback = types.model('Feedback',{
    id : types.string,
    name : types.string,
    description : types.string,
    createdOn : types.string
})

const FeedbackStore= types.model({
    Feedbacks : types.array(feedback)
})
.actions(self=>({
    async load(){
        var response = await getData(API_URL+'api/feedback/index');
        if(response.ok){
            var feedbacks = response.data;
            self.updateFeedbacks(feedbacks);
        }
    },
    updateFeedbacks(feedbacksToUpdate){
        self.Feedbacks = feedbacksToUpdate;
    }
}))
.create({
    Feedbacks : []
})

export default FeedbackStore;