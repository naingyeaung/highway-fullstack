import { types } from "mobx-state-tree";
import AsyncStorage from "@react-native-community/async-storage";


const note = types.model('note', {
    id: types.number,
    title: types.string,
    description: types.string
});

const NoteStore = types.model({
    Notes : types.array(note)
})
.actions(self=>({
    async load(){
        var result = await AsyncStorage.getItem('notes');
        var notes = JSON.parse(result);
        console.log(notes);
        self.updateNotes(notes);
    },
    updateNotes(notesToUpdate){
        self.Notes = notesToUpdate;
    }
}))
.create({
    Notes : []
})

export default NoteStore;