import { types } from "mobx-state-tree";
import AsyncStorage from "@react-native-community/async-storage";

const favorite = types.model('favorite',{
    id : types.string,
    name : types.string,
    description : types.string,
    photo : types.string
});

const FavoriteStore = types.model({
    Favorites : types.array(favorite)
})
.actions(self=>({
    async load(){
        var responses = await AsyncStorage.getItem('favorites');
        var favorites = JSON.parse(responses);
        console.log(favorites);
        self.updateFavorites(favorites);
    },
    updateFavorites(fToUpdate){
        self.Favorites = fToUpdate;
    }
}))
.create({
    Favorites : []
})

export default FavoriteStore;