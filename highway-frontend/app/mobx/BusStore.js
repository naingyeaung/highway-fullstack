import { types } from "mobx-state-tree";
import AsyncStorage from "@react-native-community/async-storage";
import { getData } from "../services/fetch";
import { API_URL } from "../components/common";
import CityStore from "./CityStore";

const Bus = types.model('Bus',{
    id : types.string,
    name : types.string,
    description : types.string,
    photo : types.string
})

const BusStore = types.model({
    Buses : types.array(Bus),
    SearchContex : types.string
})
.actions(self=>({
    async load(){
        var response = await getData(API_URL+'api/bus/searchBus?id='+CityStore.CurrentCity+'&name='+self.SearchContex);
        if(response.ok){
            var buses = response.data;
            self.updateBuses(buses);
        }else{
            console.log(response.data);
        }
    },
    updateBuses(busesToUpdate){
        self.Buses = busesToUpdate;
    },
    updateSearchContext(context){
        self.SearchContex = context;
    }
}))
.create({
    Buses:[],
    SearchContex : ''
})

export default BusStore;