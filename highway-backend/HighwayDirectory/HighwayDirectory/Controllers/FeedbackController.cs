﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HighwayDirectory.Entities;
using HighwayDirectory.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace HighwayDirectory.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class FeedbackController : ControllerBase
    {
        private readonly ApplicationDbContext _context;

        private readonly IConfiguration _configuration;

        public FeedbackController(ApplicationDbContext context, IConfiguration configuration)
        {
            _context = context;
            _configuration = configuration;
        }

        public IActionResult Index()
        {
            var feedbacks = _context.Feedbacks;

            return Ok(feedbacks);
        }

        public IActionResult GetPopularFeedbacks()
        {
            var feedbacks = _context.Feedbacks.Take(3);

            return Ok(feedbacks);
        }

        public IActionResult Create(Feedback feedback)
        {
            feedback.Id = Guid.NewGuid();
            feedback.CreatedOn = DateTime.UtcNow.AddHours(6).AddMinutes(30);
            _context.Feedbacks.Add(feedback);

            _context.SaveChanges();
            return Ok(feedback);
        }
    }
}