﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HighwayDirectory.Entities;
using HighwayDirectory.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace HighwayDirectory.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class BusController : ControllerBase
    {
        private readonly ApplicationDbContext _context;

        private readonly IConfiguration _configuration;

        public BusController(ApplicationDbContext context, IConfiguration configuration)
        {
            _context = context;
            _configuration = configuration;
        }

        [HttpGet]
        public IActionResult Index()
        {
            var buses = _context.Buses;

            return Ok(buses);
        }

        public IActionResult GetPopularBuses()
        {
            var buses = _context.Buses.Take(3);

            return Ok(buses);
        }

        [HttpGet]
        public IActionResult SearchBusByName(string name)
        {
            var buses = _context.Buses.Where(x => x.Name.Contains(name));

            return Ok(buses);
        }

        public IActionResult SearchBusByCity(Guid id)
        {
            List<Bus> buses = new List<Bus>();

            var busIds = _context.BusTransportCities.Where(x => x.CityId == id);

            foreach(var busId in busIds)
            {
                Bus bus = _context.Buses.Where(x => x.Id == busId.BusId).FirstOrDefault();
                if (bus != null)
                {
                    buses.Add(bus);
                }
            }

            return Ok(buses);
        }

        public IActionResult SearchBus([FromQuery]string id, [FromQuery]string name)
        {
            List<Bus> buses = new List<Bus>();
            var busIds = id == ""||id==null ? _context.Buses.Select(x => x.Id) : _context.BusTransportCities.Where(x => x.CityId == new Guid(id)).Select(x => x.BusId);
            foreach(var busId in busIds)
            {
                Bus bus = _context.Buses.Where(x => x.Id == busId).FirstOrDefault();
                if (bus != null)
                {
                    buses.Add(bus);
                }
            }

            buses = name == "" || name==null ? buses : buses.Where(x => x.Name.Contains(name)).ToList();

            return Ok(buses);
        }

        public IActionResult Create(BusCreate bus)
        {
            Bus b = new Bus();
            b.Id = Guid.NewGuid();
            b.Name = bus.Name;
            b.Description = bus.Description;
            b.Photo = bus.Photo;
            _context.Buses.Add(b);
            _context.SaveChanges();

            foreach (var cityId in bus.CityIds)
            {
                BusTransportCity bsc = new BusTransportCity();
                bsc.Id = Guid.NewGuid();
                bsc.BusId = b.Id;
                bsc.CityId = cityId;

                _context.BusTransportCities.Add(bsc);
            }
            _context.SaveChanges();
            return Ok();
        }

        public IActionResult Detail([FromQuery]Guid id)
        {
            var bus = _context.Buses.Where(x => x.Id == id).FirstOrDefault();
            List<City> cities = new List<City>();
            if (bus != null)
            {
                var citieIds = _context.BusTransportCities.Where(x => x.BusId == bus.Id);

                foreach(var cityId in citieIds)
                {
                    var city = _context.Cities.Where(x => x.Id == cityId.CityId).FirstOrDefault();
                    cities.Add(city);
                }
                var data = new
                {
                    bus,
                    cities
                };

                return Ok(data);
            }
            else
            {
                return Ok();
            }
        }

    }
}