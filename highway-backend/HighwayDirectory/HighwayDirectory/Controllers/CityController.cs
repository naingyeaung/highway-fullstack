﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HighwayDirectory.Entities;
using HighwayDirectory.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace HighwayDirectory.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class CityController : ControllerBase
    {
        private readonly ApplicationDbContext _context;

        private readonly IConfiguration _configuration;

        public CityController(ApplicationDbContext context, IConfiguration configuration)
        {
            _context = context;
            _configuration = configuration;
        }

        public IActionResult Index()
        {
            var cities = _context.Cities;

            return Ok(cities);
        }

        public IActionResult Create(City city)
        {
            city.Id = Guid.NewGuid();
            _context.Cities.Add(city);

            _context.SaveChanges();

            return Ok();
        }

        [HttpGet]
        public IActionResult SearchCity([FromQuery]string search)
        {
            var cities = search!=""&&search!=null?_context.Cities.Where(x => x.Name.Contains(search)):_context.Cities;

            return Ok(cities);
        }
    }
}