﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace HighwayDirectory.Models
{
    public class BusTransportCity
    {
        public Guid Id { get; set; }

        public Guid CityId { get; set; }

        public Guid BusId { get; set; }

        //foreignkey table
        [ForeignKey("CityId")]
        public City City { get; set; }

        [ForeignKey("BusId")]
        public Bus Bus { get; set; }

        //constructor
        public BusTransportCity()
        {

        }
    }
}
