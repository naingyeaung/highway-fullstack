﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HighwayDirectory.Models
{
    public class City
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string Url { get; set; }

        //constructor
        public City()
        {

        }
    }
}
